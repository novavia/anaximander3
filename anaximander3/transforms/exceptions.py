"""
This module defines custom exceptions for Anaximander's transforms package.

This module is part of the Anaximander project.
Copyright (C) Novavia Solutions, LLC.
"""


class OperationalError(Exception):
    """A customized exception for transfomation / streaming errors."""
    pass
